""" Settings for S3 Meta Data Consumer """

import os
from pathlib import Path

try:
    S3_BUCKETS = os.environ.get('S3_BUCKETS').split(',')
except:
    print("Error: S3 Bucket List cannot be empty")
    exit()

AWS_ACCESS_KEY_ID = os.environ.get('AWS_ACCESS_KEY_ID')
AWS_SECRET_ACCESS_KEY = os.environ.get('AWS_SECRET_ACCESS_KEY')

is_aws_configured = os.path.isfile(os.path.join(str(Path.home()), '.aws/config'))

if not any([all([AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY]), is_aws_configured]):
    print("Error: AWS Credentials not configured")
    exit()

ES_HOST = os.environ.get('ELASTIC_SEARCH_HOST', 'http://localhost:9200')

COLLECTOR_EVENT_ID = os.environ.get('S3_META_CONSUMER', 'S3_META_CONSUMER')

COLLECTOR_QUEUE_NAME = os.environ.get('S3_EVENTS_QUEUE', 'S3_EVENTS_QUEUE')
