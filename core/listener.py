import traceback

from sqs_listener import SqsListener

from core.index import add_data_to_index, delete_data_from_index
from core.s3_crud import handle_client_delete, handle_client_update


class MyListener(SqsListener):
    """
    SQS Listener Class
    :param SqsListener: SqsListener Object
    :type SqsListener: Object
    """

    def handle_message(self, body, attributes, messages_attributes):
        event_from = body.get('event_from', 'S3')
        mappings = {
            'S3': self._handle_S3,
            'botosql_client': self._handle_botosql_client
        }
        mappings[event_from](body)

    def _handle_S3(self, body):
        try:
            mappings = {
                    'ObjectRemoved:Delete': delete_data_from_index,
                    'ObjectCreated:Put': add_data_to_index,
            }
            event_name = body["Records"][0]["eventName"]
            mappings[event_name](body)
        except:
            traceback.print_exc()

    def _handle_botosql_client(self, body):
        try:
            mappings = {
                'update': handle_client_update,
                'delete': handle_client_delete
            }
            event_name = body['event_name']
            mappings[event_name](body)
        except:
            traceback.print_exc()
