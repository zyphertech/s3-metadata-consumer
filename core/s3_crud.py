from core.util import get_boto_client, get_aws_session

def handle_client_update(body):
    keys_to_update = body['keys_to_update']
    bucket = body['bucket_name']
    metadata = body['metadata']
    session = get_aws_session()
    s3 = session.resource('s3')
    for key in keys_to_update:
        s3_object = s3.Object(bucket, key)
        metadata.update(s3_object.metadata)
        s3_object.put(Metadata=metadata)
        print("Metadata updated for key {}".format(key))

def handle_client_delete(body):
    keys_to_delete = body['keys_to_delete']
    bucket = body['bucket_name']
    delete_payload = {"Objects": [{"Key": key} for key in keys_to_delete], 'Quiet': False}
    client = get_boto_client("s3")
    client.delete_objects(Bucket=bucket, Delete=delete_payload)
