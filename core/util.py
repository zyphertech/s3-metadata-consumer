import boto3

import settings


def get_aws_session():
    """
    creates an AWS session with credentials
    :return: boto s3 session object
    :rtype: boto session object
    """

    if settings.AWS_ACCESS_KEY_ID and settings.AWS_SECRET_ACCESS_KEY:
        return boto3.Session(
            aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
            aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
        )
    return boto3.Session()


def get_boto_client(service_name):
    """
    creates a boto client using credentials
    :param service_name: name of the service
    :type service_name: string
    :return: boto client
    :rtype: boto client object
    """

    kwargs = {
        'service_name': service_name,
    }
    if settings.AWS_ACCESS_KEY_ID and settings.AWS_SECRET_ACCESS_KEY:
        kwargs['aws_access_key_id'] = settings.AWS_ACCESS_KEY_ID
        kwargs['aws_secret_access_key'] = settings.AWS_SECRET_ACCESS_KEY

    return boto3.client(**kwargs)
