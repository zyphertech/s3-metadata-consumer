from elasticsearch import Elasticsearch

from core.util import get_boto_client, get_aws_session

import settings

es = Elasticsearch(settings.ES_HOST)


def create_index():
    """
    creates index on initialization
    """

    for bucket_name in settings.S3_BUCKETS:
        if es.indices.exists(bucket_name+"_metadata"):
            print("{} Index already exists".format(bucket_name+"_metadata"))
            continue
        request_body = {
            "settings": {
                "number_of_shards": 1,
                "number_of_replicas": 0
                }
            }
        try:
            es.indices.create(index=bucket_name+"_metadata", body=request_body)
            print("Index created with name {}".format(bucket_name+"_metadata"))
        except Exception as e:
            print(e)


def add_data_to_index(data):
    """
    Adds metadata of the object in index, when it is created or updated
    :param data: metadata of the object
    :type data: dictionary
    """
    key = data["Records"][0]["s3"]["object"]["key"]
    bucket_name = data["Records"][0]["s3"]["bucket"]["name"]
    session = get_aws_session()
    s3 = session.resource("s3")
    s3_object = s3.Object(bucket_name, key)

    metadata = s3_object.metadata
    last_updated = s3_object.last_modified
    content_type = s3_object.content_type
    size = s3_object.content_length
    body = {
        "metadata": metadata,
        "last_updated": last_updated,
        "content_type": content_type,
        "size": size,
    }
    res = es.index(index=bucket_name+"_metadata", doc_type='METADATA', id=key, body=body)
    print(res["result"])


def delete_data_from_index(data):
    """
    Deletes data from index when the object is deleted
    :param data: metadata of the object
    :type data: dictionary
    """

    key = data["Records"][0]["s3"]["object"]["key"]
    bucket_name = data["Records"][0]["s3"]["bucket"]["name"]
    res = es.delete(index=bucket_name+"_metadata", doc_type='METADATA', id=key)
    print(res["result"])
