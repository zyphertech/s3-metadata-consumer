from datetime import datetime
import json
import boto3
import os

from core.util import get_aws_session, get_boto_client

import settings

session = get_aws_session()


def check_if_all_given_bucket_exists():
    """
    checks whether the buckets list specified by the user
    exists on AWS or not if the bucket exists then it prints
    Bucket validation success otherwise it would raise Exception
    that particular bucket is not found
    """

    s3 = session.resource('s3')
    account_buckets = [bucket.name for bucket in s3.buckets.all()]
    for bucket in settings.S3_BUCKETS:
        if bucket not in account_buckets:
            raise Exception("Bucket with name {} not found".format(bucket))
    print("Bucket Validation success")


def get_bucket_notifications(bucket_name):
    """
    fetches the notification configuration of S3 Bucket
    :param bucket_name: name of the bucket
    :type bucket_name: string
    :return: Notification configuration
    :rtype: list
    """

    client = get_boto_client('s3')
    try:
        return client.get_bucket_notification_configuration(Bucket=bucket_name)['QueueConfigurations']
    except KeyError:
        return []


def is_sqs_configured(bucket_name):
    """
    checks whether the S3 Bucket's SQS Notification is
    configured or not
    :param bucket_name: name of the bucket
    :type bucket_name: string
    :return: True/False
    :rtype: Boolean
    """

    notifications = get_bucket_notifications(bucket_name)
    for notification in notifications:
        if settings.COLLECTOR_EVENT_ID in notification['Id']:
            return True
    return False


def queue_exists():
    """
    checks whether the SQS queue exists on AWS or not
    and returns value
    :return: True/False, url
    :rtype: boolean, string
    """

    client = get_boto_client('sqs')
    try:
        queue_urls = client.list_queues()['QueueUrls']
    except KeyError:
        return {
            'exists': False
        }

    for url in queue_urls:
        if settings.COLLECTOR_QUEUE_NAME in url:
            return {
                'exists': True,
                'url': url,
            }
    return {
        'exists': False
    }


def create_queue():
    """
    creates SQS queue on AWS and returns a queue url
    :return: queue url
    :rtype: string
    """

    client = get_boto_client('sqs')
    print("Creating queue with name {}".format(settings.COLLECTOR_QUEUE_NAME))
    return client.create_queue(QueueName=settings.COLLECTOR_QUEUE_NAME)['QueueUrl']


def update_policy(queue_url):
    """
    configure the SQS queue policy
    :param queue_url: url of the queue
    :type queue_url: string
    """

    client = get_boto_client('sqs')
    account_id = get_boto_client('sts').get_caller_identity().get('Account')
    policy = {}
    policy['Statement'] = [
        {
            "Sid": 'S3_META_CONSUMER_{}'.format(index),
            "Effect": "Allow",
            "Principal": "*",
            "Action": "SQS:SendMessage",
            "Resource": "arn:aws:sqs:{}:{}:{}".format(session.region_name, account_id, settings.COLLECTOR_QUEUE_NAME),
            "Condition": {
                "ArnEquals": {
                    "aws:SourceArn": [
                        get_current_user_arn(),
                        "arn:aws:s3:*:*:{}".format(bucket_name)
                    ]
                }
            }
        }
        for index, bucket_name in enumerate(settings.S3_BUCKETS)
    ]
    print('Updating Queue Policy with value {}'.format(json.dumps(policy)))
    client.set_queue_attributes(QueueUrl=queue_url, Attributes={"Policy": json.dumps(policy)})


def configure_events_for_bucket(bucket_name):
    """
    configure the S3 Buckets to send notifications
    :param bucket_name: name of the bucket
    :type bucket_name: string
    """

    s3 = session.resource('s3')
    notification_queue_configurations = [
        {
            'Id': (settings.COLLECTOR_EVENT_ID + '__' + str(datetime.now())).replace(' ', '_'),
            'QueueArn': 'arn:aws:sqs:us-east-1:026444216171:S3_EVENTS_QUEUE',
            'Events': [
                's3:ObjectCreated:*',
                's3:ObjectRemoved:*',
            ]
        }
    ]
    s3.BucketNotification(bucket_name=bucket_name).put(NotificationConfiguration={
        "QueueConfigurations": notification_queue_configurations
    })


def configure_sqs_for_bucket(bucket_name):
    """
    checks whether SQS is configured with bucket or not
    :param bucket_name: name of the bucket
    :type bucket_name: string
    """

    response = queue_exists()
    if not response['exists']:
        queue_url = create_queue()
    else:
        queue_url = response['url']

    update_policy(queue_url)
    configure_events_for_bucket(bucket_name)


def configure_sqs_for_all_buckets():
    """
    configures SQS for all the buckets
    """

    for bucket in settings.S3_BUCKETS:
        if is_sqs_configured(bucket):
            print("SQS already configured for bucket {}".format(bucket))
            continue
        else:
            configure_sqs_for_bucket(bucket)

def get_current_user_arn():
    client = boto3.client('sts')
    caller_identity = client.get_caller_identity()
    return caller_identity['Arn']

def get_account_id():
    client = boto3.client('sts')
    caller_identity = client.get_caller_identity()
    return caller_identity['Account']

def set_account_id():
    client = boto3.client('sts')
    caller_identity = client.get_caller_identity()
    os.environ['AWS_ACCOUNT_ID'] = get_account_id()
