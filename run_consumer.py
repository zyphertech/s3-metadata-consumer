from core.configure import check_if_all_given_bucket_exists
from core.configure import configure_sqs_for_all_buckets
from core.configure import set_account_id
from core.index import create_index
from core.listener import MyListener
# from interface.interface import search

import settings


def validate():
    check_if_all_given_bucket_exists()


def run():
    validate()
    configure_sqs_for_all_buckets()
    create_index()
    set_account_id()
    listener = MyListener(settings.COLLECTOR_QUEUE_NAME, error_queue='my-error-queue', region_name='us-east-1')
    listener.listen()


if __name__ == '__main__':
    run()
